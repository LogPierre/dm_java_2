import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;


public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
      return a + b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
    	Integer mini = null;
    	if(!(liste.isEmpty())) {
    		mini = liste.get(0);
    		for(int i = 1; i < liste.size(); ++i) {
    			if(liste.get(i) < mini) {
    				mini = liste.get(i); }
        		}
        	}
      return mini;
      }


    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
      Boolean plusPetit = true;
      if(!(liste.isEmpty())) {
          for(T elem : liste) {
            String li = (String) elem;
            String val = (String) valeur;
            if(!(li.length() < val.length())) {
              plusPetit = false;  }
        }
      }
      return plusPetit;
    }



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
      List<T> liste0 = new ArrayList<>();
      List<T> res = new ArrayList<>();
      if(!(liste1.isEmpty() && liste2.isEmpty())) {
        for(int i = 0; i < liste1.size(); ++i) {
          liste0.add(liste1.get(i)); }
        for(int i = 0; i < liste2.size(); ++i) {
          liste0.add(liste2.get(i)); }
        Collections.sort(liste0);
        int cpt=0;
        T memo = null;
        for(T li : liste0) {
          if(memo == null) {
            memo = li;
            cpt += 1; }
          else {
            if(memo == li) {
              cpt += 1;
              if(cpt == 2) {
                res.add(li); }
            }
            if(memo != li){
              cpt = 1;}
            memo = li; }
        }
    }
      return res;
    }


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
      List<String> listeDesMots = new ArrayList<>();
      if(!(texte.isEmpty())) {
        String listeMots = "";
        for(int i = 0; i < texte.length(); ++i) {
          if(!(texte.charAt(i) == ' ')) {
          listeMots += texte.charAt(i);
          }
          else if(!(listeMots.equals(""))) {
            listeDesMots.add(listeMots);
            listeMots = ""; }
        }
        if(!(listeMots.equals(""))) {
          listeDesMots.add(listeMots);
        }
      }
      return listeDesMots;
     }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, renvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
      Map<String, Integer> dicoTexte = new HashMap<>();
      List<String> motText = new ArrayList<>(decoupe(texte));
      // Collections.sort(motText);
      String motMaj = null;
      int val = 0;
      if(!(texte.length() == 0)) {
        for(String mot : motText) {
          if(dicoTexte.containsKey(mot)) {
            val = dicoTexte.get(mot);
            dicoTexte.put(mot, val + 1);
          }
          else {
            dicoTexte.put(mot, 1);
          }
        }
        for(String clef : dicoTexte.keySet()) {
          if(motMaj == null) {
            motMaj = clef;
          }
          else if(dicoTexte.get(motMaj) <= dicoTexte.get(clef)) {
            if(dicoTexte.get(motMaj) == dicoTexte.get(clef)) {
              if(motMaj.compareTo(clef) > 0) {
                motMaj = clef;
              }
            }
            else {
              motMaj = clef;
            }
          }
        }
      }
      return motMaj;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
      Boolean parenthesee = false;
      Boolean bienOuv = false;
      int parenOuv = 0;

      if(!(chaine == "")) {
        if(!(chaine.charAt(0)== ')')) {
          // if(!(chaine.charAt(-1)== '(')) {
          for(Character car : chaine.toCharArray()) {
            if(car.equals('(')) {
              parenOuv += 1;
              bienOuv = true;
            } else {
              parenOuv -= 1;
              bienOuv = false;}
            }
          if((parenOuv == 0) && !(bienOuv)) { parenthesee = true;}
          }
        }
      else { parenthesee = true;}
      return parenthesee;
    }

    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
      Boolean parenthCroch = false;
      Boolean bienPar = false;
      Boolean bienCro = false;
      int paren = 0;
      int croch = 0;
      if(!(chaine == "")) {
        if((chaine.charAt(0)!= ')') && (chaine.charAt(0)!= ']')) {
          for(Character car : chaine.toCharArray()) {
            if(car.equals('(')) {
              paren += 1;
              bienPar = true;
            }
            else if(car.equals(')')){
              paren -= 1;
              bienPar = false;
            }
            else if(car.equals('[')) {
              croch += 1;
              bienCro = true;
            }
            else if(car.equals(']')){
              croch -= 1;
              bienCro = false;}
            }
          if((paren == 0) && (croch == 0) && !(bienPar) && !(bienCro)) { return true;}
        }
        if(chaine.charAt(0) != '(' && chaine.charAt(0) != '[') { return false;}
        if(chaine.charAt(chaine.length()-1) != ')' && chaine.charAt(chaine.length()-1) != ']') { return false;}
        }
      else { parenthCroch = true;}
      return parenthCroch;
    }


    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
      boolean trouve = false;
      int indDeb = 0;

      if(!liste.isEmpty()) {
        int indFin = (liste.size()-1);
        while(!(trouve) && (indDeb <= indFin)) {
            int indMil = (indDeb + indFin)/2;
            if(liste.get(indMil) == valeur) {
                trouve = true;
            }
            else if(liste.get(indMil) > valeur) {
              indFin = indMil - 1;
            }
            else {
                indDeb = indMil + 1; }
        }
      }
      return trouve;
    }

}
